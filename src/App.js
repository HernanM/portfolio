import React from 'react';
import "./App.css"
import "./css/tailwind.css"
import Presentation from "./Components/Presentation/Index";
import SkillsContainer from "./Components/Skills/SkillsScontainer";
import ProjectsContainer from "./Components/Projects/ProjectContainer";
import Contact from "./Components/Contact/Index";


const name = "Hernan";
const imgUrl = "https://i.pinimg.com/originals/0a/ec/a1/0aeca1c86f5d2a198c1e6d9b5024f851.jpg";
const description = "Soy estudiante de ingenieria en sistemas de informacion y de desarollo full stack. ";
const mySkills = [{ name: "HTML", level: 4 }, { name: "CSS", level: 3 }, { name: "JavaScript", level: 2 }, { name: "React", level: 1 }]
const myProjects = [{ name: "infobae", imgUrl: "https://i.ytimg.com/vi/DpWYkh4_tc4/maxresdefault.jpg", repoUrl: "https://gitlab.com/HernanM/infobae" }, { name: "otro", imgUrl: "https://i.ytimg.com/vi/DpWYkh4_tc4/maxresdefault.jpg" }]


function App() {
  return (

    <div className="flex flex-col text-gray-800 bg-white tipografiaPrincipal">

      <div className="p-4">

        <Presentation
          img={imgUrl}
          name={name}
          description={description}
        />

        <SkillsContainer skills={mySkills} />
        <ProjectsContainer projects={myProjects} />



      </div>
    </div>
  );
}

export default App;
