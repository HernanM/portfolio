import React ,{Component} from "react";
import Skill from "./Skill";



export default class SkillsContainer extends Component{
    render(){
        const {skills} = this.props;

        return(
            <div className="flex flex-col mb-5">
                <h1>Mis conocimientos</h1>
                { skills.map( skill => <Skill info={skill} key={skill.name} />) }
            </div>
        );
    }
}