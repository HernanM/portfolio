import React, { Component } from "react";

export default class Skill extends Component {


    getLevelStyle(levelNumber) {
        return (levelNumber === 1 ? { porcentage: "25%", color: "bg-red-400" } : levelNumber === 2 ? { porcentage: "50%", color: "bg-yellow-400" } : levelNumber === 3 ? { porcentage: "75%", color: "bg-blue-400" } : { porcentage: "100%", color: "bg-green-400" });
    }

    render() {
        const { name, level } = this.props.info;
        const levelStyle = this.getLevelStyle(level);

        return (
            <div className="w-full my-1">

                <div className="shadow w-full bg-gray-100 ">
                    <div className={`${levelStyle.color} uppercase font-bold leading-tight py-1 text-white h-6 `} style={{ width: levelStyle.porcentage }}><pre> {name} </pre></div>
                </div>

            </div>
        );
    }
}