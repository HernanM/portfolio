import React, { Component } from 'react';
import "./style.css"

export default class Index extends Component {

    render() {
        const { img, name, description } = this.props;

        return (

            <div className="mb-5">


                <div className="-m-4">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#FC8181" fill-opacity="1" d="M0,224L48,186.7C96,149,192,75,288,48C384,21,480,43,576,90.7C672,139,768,213,864,256C960,299,1056,309,1152,298.7C1248,288,1344,256,1392,240L1440,224L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"></path></svg>
                </div>

                <div className="flex flex-row items-center mb-5">

                    <div className="w-1/2 flex justify-center">
                        <img style={{ zIndex: 99999 }} className="shadow rounded-full h-48 w-48 " src={img} alt="" />
                    </div>

                    <div className="flex w-1/2 justify-center ">

                        <div style={{ zIndex: 99999 }}>
                            <h4 className="text-4xl uppercase font-bold"> Hola </h4>
                            <h1 className="" >Soy {name}</h1>
                        </div>

                    </div>

                </div>

                <p className="text-center"> {description} </p>


                <div className="-m-4">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#ff5500" fill-opacity="1" d="M0,0L48,26.7C96,53,192,107,288,144C384,181,480,203,576,176C672,149,768,75,864,48C960,21,1056,43,1152,85.3C1248,128,1344,192,1392,224L1440,256L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path></svg>
                </div>
            </div>
        );
    }
}
